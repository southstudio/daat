<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="blog-post-title"><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </div>
    </div>
</div>