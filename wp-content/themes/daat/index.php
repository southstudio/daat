<?php get_header();?>

    <div class="site">

        <!-- ABOUT -->
        <div id="nosotros" class="scroll-section mb-90">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8 col-xl-6 mx-auto text-center">
                        <p class="dark-text regular-text fs-16 mxy-0">Nuestra experiencia en Marketing Estratégico y nuestra tecnología nos permite, a partir de la Data recolectada, generar modelos de crecimiento de ventas, asesoramiento en posicionamiento de marca y armado de planes de marketing potentes.</p>
                    </div>
                </div>

                <div class="row mt-90">
                    <div class="col-12 col-lg-6">
                        <div class="img-epigrafe">
                            <img class="img-fluid" src="<?php echo $GLOBALS['website_url'] ?>/wp-content/uploads/2019/12/Marcos-_Belochercovsky_CEO-scaled.jpg" alt="Daat Marketing">
                            <div class="epigrafe text-center">
                                <p class="light-text regular-text mxy-0">Marcos Belochercovsky - CEO</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 d-lg-flex flex-column">
                        <div class="my-lg-auto mt-30">
                            <p class="dark-text regular-text fs-16 mxy-0 mb-20">A través de la inteligencia de datos o BIG DATA junto a Inteligencia Artificial, analizamos las emociones de las personas, medimos el flujo de clientes en las tiendas, monitoreamos el tránsito de vehículos o contamos las personas. Así proveemos información clara y precisa sobre lo que está sucediendo con sus clientes actuales o potenciales.</p>
                            <p class="dark-text regular-text fs-16 mxy-0">Ayudamos a identificar el comportamiento de los consumidores y conocer a cuál segmento pertenecen para poder ser más asertivo con su target. Generamos insights a partir del análisis de los datos recolectados.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- PRODUCTS -->
        <div id="productos" class="scroll-section">
            <div class="blue-section products-container">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h1 class="primary-text bolder-text mxy-0 mb-50 mt-50 fs-30">Productos</h1>
                        </div>
                    </div>

                    <div class="row mb-20 mb-lg-40">
                        <?php
                            $args = array(
                                'post_type'     => 'daat_products',
                                'category_name' => 'Featured'
                            );
                            $query_products_featured = new WP_Query($args);
                            if ( $query_products_featured->have_posts() ) :
                            while ( $query_products_featured->have_posts() ) : $query_products_featured->the_post();
                        ?>
                            <div class="col-12 col-lg-4 featured-product align-self-stretch" data-aos="fade-up">
                                <div class="product-holder daat-card">
                                    <h1 class="bolder-text dark-text mxy-0 mb-20 fs-20"><?php the_title(); ?></h1>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php 
                            endwhile;
                            wp_reset_postdata();
                            endif;
                        ?>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div id="productsCarousel" class="carousel slide daat-carousel products-carousel p-static" data-ride="carousel" data-aos="fade-up" data-aos-delay="600">
                                <div class="daat-card">
                                    <div class="carousel-inner">
                                        <?php
                                            $args = array(
                                                'post_type'     => 'daat_products',
                                                'category_name' => 'Slider'
                                            );
                                            $query_products_slider = new WP_Query($args);
                                            if ( $query_products_slider->have_posts() ) :
                                            while ( $query_products_slider->have_posts() ) : $query_products_slider->the_post();
                                            $imagenProducto  = get_field( 'imagen_producto' );
                                            $imagenDestacada = get_field( 'imagen_destacada' );
                                            $videoDestacado  = get_field( 'video_destacado' );
                                        ?>
                                            <div class="carousel-item">
                                                <div class="row">
                                                    <div class="col-12 col-lg-6">
                                                        <?php if ($imagenDestacada && in_array('Imagen', $imagenProducto)) : ?>
                                                            <img class="img-fluid" src="<?php echo $imagenDestacada; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                                        <?php elseif ($videoDestacado && in_array('Video', $imagenProducto)) : ?>
                                                            <video class="carousel-video" width="100%" muted="muted" controls loop autoplay>
                                                                <source src="<?php echo $videoDestacado; ?>" type="video/mp4">
                                                                Your browser does not support the video tag.
                                                            </video>
                                                        <?php endif; ?>
                                                    </div>
                                                    <div class="col-12 col-lg-6 d-lg-flex flex-column">
                                                        <div class="my-lg-auto mt-30">
                                                            <h1 class="bolder-text dark-text mxy-0 mb-20 fs-20"><?php the_title(); ?></h1>
                                                            <?php the_content(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php 
                                            endwhile;
                                            wp_reset_postdata();
                                            endif;
                                        ?>
                                    </div>
                                    <a class="carousel-control carousel-control-prev" href="#productsCarousel" role="button" data-slide="prev">
                                        <i class="fas fa-chevron-left fs-20 primary-text"></i>
                                    </a>
                                    <a class="carousel-control carousel-control-next" href="#productsCarousel" role="button" data-slide="next">
                                        <i class="fas fa-chevron-right fs-20 primary-text"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- BANNER -->
        <div class="banner pt-60 pb-60">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8 col-xl-6 mx-auto text-center">
                        <h1 class="fs-30 light-text bolder-text mxy-0 mb-40">Ahora es posible llevar las métricas del mundo digital a la publicidad en el mundo físico.</h1>
                        <p class="fs-16 light-text regular-text mxy-0">Logre comprender el comportamiento, las emociones y los datos demográficos de quienes ven la publicidad. Obtenga datos en tiempo real e insights de comportamiento de consumo en todo tipo de formato de OOH (Out of Home) y DOOH (Digital Out of Home).</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- OUT OF HOME -->
        <div id="ooh" class="scroll-section blue-section">
            <div class="ooh-section">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <h1 class="dark-text bolder-text fs-20 mxy-0 mt-20 mb-20">Medición de audiencias</h1>
                            <p class="dark-text regular-text fs-16 mxy-0 mb-20">La tecnología está impulsando y acelerando el crecimiento de la publicidad en la Vía Pública. Al utilizar las soluciones de inteligencia artificial y visión computarizada, ahora es posible obtener datos instantáneos e hiperrealistas directamente desde el soporte publicitario. Contar con esta solución, promueve a las marcas aumentar exponencialmente la inversión en publicidad en Vía Pública ya que pueden contar con el retorno de su inversión.</p>
                            <p class="dark-text regular-text fs-16 mxy-0">A partir de ahora los grandes jugadores de la industria de la publicidad en Vía Pública (Out Of Home - OOH) pueden vender sus soportes y sus inventarios, con la prueba irrefutable de  que logran llegar a la audiencia exacta que desea el anunciante. Esta tecnología permite decirles con una amplia granularidad, por ejemplo el número exacto de mujeres entre 25 y 40 años que miraron el soporte publicitario, por cuanto tiempo lo hicieron y como se sintieron al verlo.</p>
                        </div>

                        <div class="col-12 col-lg-6 d-lg-flex flex-column">
                            <div class="my-lg-auto mt-30">
                                <img class="img-fluid" src="<?php echo $GLOBALS['website_url'] ?>/wp-content/uploads/2019/12/medicion_de_audencias_ppal.png" alt="Daat Marketing">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-90">
                        <div class="col-12 col-lg-6 d-lg-flex flex-column order-2 order-lg-1">
                            <div class="mx-auto mt-30 mt-lg-0">
                                <img class="img-fluid" src="<?php echo $GLOBALS['website_url'] ?>/wp-content/uploads/2019/12/medicion_de_audencias_01.png" alt="Daat Marketing">
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 order-1 order-lg-2">
                            <p class="dark-text regular-text fs-16 mxy-0 mb-20">Instalación de dispositivos especiales y/o cámaras inteligentes en cualquier punto de contacto con los clientes. Estas cámaras recolectan la data, luego es procesada por un algoritmo de inteligencia artificial y vuelca los datos en un dashboard de forma instantánea y de lectura intuitiva.</p>
                        </div>
                    </div>
                </div>

                <div class="white-divider"></div>

                <div class="container">
                    <div class="row mt-90">
                        <div class="col-12 col-lg-6">
                            <p class="dark-text regular-text fs-16 mxy-0 mb-20">Instalación de dispositivos especiales y/o cámaras inteligentes en cualquier punto de contacto con los clientes. Estas cámaras recolectan la data, luego es procesada por un algoritmo de inteligencia artificial y vuelca los datos en un dashboard de forma instantánea y de lectura intuitiva.</p>
                        </div>

                        <div class="col-12 col-lg-6 d-lg-flex flex-column">
                            <div class="mx-auto mt-30 mt-lg-0">
                                <img class="img-fluid" src="<?php echo $GLOBALS['website_url'] ?>/wp-content/uploads/2019/12/medicion_de_audencias_02.png" alt="Daat Marketing">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- USES -->
        <div id="usos" class="scroll-section usos-container">

            <div class="container mt-90">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="primary-text bolder-text mxy-0 mb-30 fs-30">Usos</h1>
                    </div>
                </div>

                <!--<div class="d-md-flex align-items-center justify-content-between flex-wrap">-->
                <div class="row">
                    <?php
                        $args = array(
                            'post_type' => 'daat_uses',
                        );
                        $query_uses = new WP_Query($args);
                        if ( $query_uses->have_posts() ) :
                        while ( $query_uses->have_posts() ) : $query_uses->the_post();
                    ?>
                        <div class="col-6 col-lg-3 use-col align-self-stretch mb-40 mb-lg-70">
                            <div class="use-holder daat-card text-center mx-auto d-flex flex-column">
                                <div class="mx-auto my-auto">
                                    <?php the_post_thumbnail(); ?>
                                    <p class="regular-text dark-text mxy-0 mt-20 fs-16"><?php the_title(); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php 
                        endwhile;
                        wp_reset_postdata();
                        endif;
                    ?>
                </div>
            </div>
            
        </div>

        <!-- CONTACT -->
        <div class="blue-section">
            <div class="container">
                <div class="row">
                    <div id="contact" class="scroll-section contact-container col-12 text-center">
                        <h1 class="primary-text bolder-text mxy-0 mb-40 fs-30">Contacto</h1>

                        <div class="daat-card mx-auto text-left" data-aos="fade-up" data-aos-delay="600">
                            <div class="form-container">
                                <?php echo do_shortcode('[contact-form-7 id="38" title="Contact form"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php get_footer();?>