<?php
    // Products
    add_action( 'init', 'cpt_products' );
    function cpt_products() {
        $labels = array(
            'name'               => __( 'Products' ),
            'singular_name'      => __( 'Product' ),
            'menu_name'          => __( 'Products' ),
            'all_items'          => __( 'All Products' ),
            'insert_into_item'   => __( 'Insert in product' ),
            'add_new'            => __( 'Add new product' ),
            'add_new_item'       => __( 'Add new product' )
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'editor' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-schedule',
            'rewrite'            => array( 'slug' => 'products' ),
            'taxonomies'         => array( 'category' ),
        );
        register_post_type( 'daat_products', $args );
    };

    // Uses
    add_action( 'init', 'cpt_uses' );
    function cpt_uses() {
        $labels = array(
            'name'               => __( 'Uses' ),
            'singular_name'      => __( 'Use' ),
            'menu_name'          => __( 'Uses' ),
            'all_items'          => __( 'All Uses' ),
            'insert_into_item'   => __( 'Insert in use' ),
            'add_new'            => __( 'Add new use' ),
            'add_new_item'       => __( 'Add new use' )
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'thumbnail' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-star-filled',
            'rewrite'            => array( 'slug' => 'uses' ),
        );
        register_post_type( 'daat_uses', $args );
    };
?>