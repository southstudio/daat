<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo get_bloginfo( 'name' );?>">
        <title><?php echo get_bloginfo( 'name' );?></title>
        <?php wp_head();?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-154551789-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-154551789-1');
        </script>
    </head>
    <body>

        <!-- https://daatmarketing.com/test -->
        <?php $GLOBALS['website_url'] = 'https://daatmarketing.com'; ?>
        
        <header>

            <div class="menu-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="navbar navbar-expand-md">
                                <?php if ( get_theme_mod( 'site_logo' ) ): ?>
                                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' )); ?>">
                                        <img class="img-fluid" src="<?php echo esc_attr(get_theme_mod( 'site_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                    </a>
                                <?php else : ?>
                                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                                <?php endif; ?>

                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTop" aria-controls="navbarTop" aria-expanded="false" aria-label="Toggle navigation">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>

                                <?php
                                    wp_nav_menu(array(
                                        'theme_location'  => 'primary',
                                        'container'       => 'div',
                                        'container_id'    => 'navbarTop',
                                        'container_class' => 'collapse navbar-collapse',
                                        'menu_class'      => 'navbar-nav ml-auto mr-auto',
                                        'add_li_class'    => 'nav-item',
                                        'add_a_class'     => 'dark-text bold-text uppercase fs-14 go-to-anchor'
                                    ));
                                ?>

                                <a href="#contact" class="daat-btn d-flex align-items-center justify-content-center nav-link go-to-anchor">Contacto</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container d-flex flex-column slider-container p-static">
                <div class="row my-auto p-static">
                    <div class="col-12 p-static">
                        <div id="headerCarousel" class="carousel slide daat-carousel header-carousel p-static" data-ride="carousel">
                            <div class="row p-static">
                                <div class="col-12 col-lg-6 col-xl-4 d-lg-flex flex-column order-2 order-lg-1">
                                    <div class="my-auto" data-aos="fade-up" data-aos-delay="200">
                                        <h1 class="primary-text bolder-text mxy-0 fs-40">Consultoría en Marketing e Innovación.</h1>
                                        <p class="primary-text regular-text mxy-0 mt-30">Apoyados en la innovación tecnológica, ayudamos a las empresas a mejorar sus decisiones estratégicas de Marketing y Comunicación.</p>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 col-xl-8 order-1 order-lg-2 mb-20 mb-lg-0 mt-20 mt-lg-0">
                                    <div class="carousel-inner">
                                        <?php
                                            $query_header                       = new WP_Query('category_name=header');
                                            while ($query_header->have_posts()) : $query_header->the_post();
                                        ?>
                                            <div class="carousel-item text-center">
                                                <?php the_post_thumbnail(); ?>
                                            </div>
                                        <?php 
                                            endwhile;
                                            wp_reset_postdata();
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control carousel-control-prev" href="#headerCarousel" role="button" data-slide="prev">
                                <i class="fas fa-chevron-left fs-20 primary-text"></i>
                            </a>
                            <a class="carousel-control carousel-control-next" href="#headerCarousel" role="button" data-slide="next">
                                <i class="fas fa-chevron-right fs-20 primary-text"></i>
                            </a>
                            <ol id="carouselIndicators" class="carousel-indicators"></ol>
                        </div>
                    </div>
                </div>
            </div>

        </header>

        <div id="siteContent">
        <!-- index.php -->