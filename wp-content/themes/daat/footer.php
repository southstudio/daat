</div><!-- #siteContent -->
        
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-8 text-center text-md-left mb-70 mb-md-0">
                        <img class="img-fluid" src="<?php echo esc_attr(get_theme_mod( 'footer_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                    </div>

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="row">
                            <div class="col-9 mx-auto ml-md-0">
                                <?php if ( get_theme_mod( 'email' ) ): ?>
                                    <a class="light-text bolder-text mxy-0 fs-14" href="mailto:<?php echo get_theme_mod( 'email' ); ?>" target="_blank">
                                        <i class="fas fa-envelope"></i>
                                        <?php echo get_theme_mod( 'email' ); ?>
                                    </a>
                                <?php endif; ?>

                                <?php if ( get_theme_mod( 'facebook-url' ) ): ?>
                                    <a class="light-text bolder-text mxy-0 fs-14 mt-10 mb-10" href="<?php echo get_theme_mod( 'facebook-url' ); ?>" target="_blank">
                                        <i class="fab fa-facebook-square"></i></i>
                                        <?php
                                            if ( get_theme_mod( 'facebook-name' ) ):
                                                echo get_theme_mod( 'facebook-name' );
                                            endif;
                                        ?>
                                    </a>
                                <?php endif; ?>

                                <?php if ( get_theme_mod( 'instagram-url' ) ): ?>
                                    <a class="light-text bolder-text mxy-0 fs-14" href="<?php echo get_theme_mod( 'instagram-url' ); ?>" target="_blank">
                                        <i class="fab fa-instagram"></i></i>
                                        <?php
                                            if ( get_theme_mod( 'instagram-name' ) ):
                                                echo get_theme_mod( 'instagram-name' );
                                            endif;
                                        ?>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <p id="backToTop" class="back-to-top light-text bold-text fs-14 mxy-0">Top <i class="fas fa-level-up-alt"></i></p>
            </div>
        </footer>
        
        <?php wp_footer(); ?> 
    </body>
</html>