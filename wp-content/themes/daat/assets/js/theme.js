jQuery(fn = $ => {
    // Global URL
    // https://daatmarketing.com/test
    globarlUrl = 'https://daatmarketing.com';

    // Contact button position on navbar
    (menuResponsive = () => {
        var navBar = document.querySelector('.navbar');
        var menu = document.getElementById('menu-menu-top');
        var contactButton = document.querySelector('.navbar .daat-btn');
        if (window.innerWidth < 768) {
            menu.appendChild(contactButton);// Append contactButton to the menu
        }
        else {
            navBar.appendChild(contactButton);// Append contactButton to the navbar
        }
    })();

    // Add active class to current menu item
    (activeClass = () => {
        document.getElementById('menu-menu-top').onclick = e => {
            if (document.querySelector('#menu-menu-top a.active') !== null) {
                document.querySelector('#menu-menu-top a.active').classList.remove('active');
            }
            e.target.classList.add('active');
        }
    })();

    // Collapse responsive navbar on click
    (collapseNavBar = () => {
        var navBarLinks = document.querySelectorAll('.navbar-collapse ul a');
        var toggler = document.querySelector('.navbar-toggler');
        if (window.innerWidth < 768) {
            navBarLinks.forEach(link => link.onclick = () => {
                // Trigger click on toggler when clicking on any link, if toggler is visible
                if (toggler.offsetWidth > 0 && toggler.offsetHeight > 0) { toggler.click(); }
            });
        }
    })();

    // Back to top action
    (backToTop = () => {
        // Not yet supported on mobile
        /*var backToTop = document.getElementById('backToTop');
        backToTop.onclick = () => {
            window.scrollBy({
                top: 0,
                behavior: 'smooth'
            });
        }*/

        $('#backToTop').click(() => {
            $("html, body").animate({ scrollTop: 0 }, 'slow');
        });
    })();

    // Navbar behavior on link click
    (navBarBehavior = () => {
        $('.go-to-anchor').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top)
            }, 1250, 'easeInOutExpo');
            event.preventDefault();
        });
    })();

    // Padding of sections with auto-scroll
    (scrollSectionHeight = () => {
        var navBarHeight = document.querySelector('.menu-wrapper').offsetHeight;
        document.querySelectorAll('.scroll-section').forEach(section => section.style.paddingTop = navBarHeight + 20 + 'px');
    })();

    // Carousel controls positions
    (carouselControls = () => {
        var negativeSpace = window.innerWidth - document.querySelector('.slider-container').offsetWidth;
        var eachNegativeSpace = negativeSpace / 2;
        if (window.innerWidth > 767) {
            document.querySelector('.carousel-control-prev').style.left = eachNegativeSpace - 20 + 'px';
            document.querySelector('.carousel-control-next').style.right = eachNegativeSpace - 20 + 'px';
        }
    })();

    // Carousel automatic indicators and active classes
    (carouselOptions = () => {
        var indicatorsDiv = document.getElementById('carouselIndicators');
        var slides = document.querySelectorAll('.header-carousel .carousel-item');
        slides.forEach((slide, index) => {
            var indicator = document.createElement('li');
            indicator.setAttribute('data-target', '#headerCarousel');
            indicator.setAttribute('data-slide-to', index);
            indicatorsDiv.appendChild(indicator);// Append new li with attributes created
            indicatorsDiv.querySelector('li').classList.add('active');// Add class 'active' to first li
        });

        // Add 'active' class to first carousel-item
        document.querySelectorAll('.daat-carousel').forEach(carousel => {
            carousel.querySelector('.carousel-item').classList.add('active');
        });

        document.querySelectorAll('.carousel-video').forEach(video => video.play());
    })();

    // Different AOS delays to each different product
    (productsAos = () => {
        var delay = 200;   
        document.querySelectorAll('.featured-product').forEach(product => {
            product.setAttribute('data-aos-delay', delay);
            delay += 200;
        });
    })();

    // Resize functions
    window.onresize = () => {
        menuResponsive();
        collapseNavBar();
        carouselControls();
        scrollSectionHeight();
    };

    // Animate on scroll
    AOS.init(); 
});