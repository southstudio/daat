<?php
    // Logo
    function site_logo( $wp_customize ) {
        // Settings
        $wp_customize->add_setting( 'site_logo' );//Setting for logo uploader

        // Controls
        // Header logo
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize,
                'site_logo',
                array(
                    'label'      => 'Upload a logo',
                    'section'    => 'title_tagline',
                    'settings'   => 'site_logo'
                )
            )
        );
    }
    add_action('customize_register', 'site_logo');

    // Social media
    function social_media( $wp_customize ){
        // Settings
        $wp_customize->add_setting( 'facebook-url', array( 'default' => '' ) );
        $wp_customize->add_setting( 'facebook-name', array( 'default' => '' ) );
        $wp_customize->add_setting( 'instagram-url', array( 'default' => '' ) );
        $wp_customize->add_setting( 'instagram-name', array( 'default' => '' ) );

        // Sections
        $wp_customize->add_section(
            'social-media',
            array(
                'title'       => __( 'Social Media', '_s' ),
                'priority'    => 30,
                'description' => __( 'Enter the URL to your accounts for each social media for the icon to appear in the header.', '_s' )
            )
        );

        // Controls
        // Facebook URL
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'facebook-url',
                array(
                    'label'    => __( 'Facebook URL', '_s' ),
                    'section'  => 'social-media',
                    'settings' => 'facebook-url'
                )
            )
        );
        // Facebook Name
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'facebook-name',
                array(
                    'label'    => __( 'Facebook Name', '_s' ),
                    'section'  => 'social-media',
                    'settings' => 'facebook-name'
                )
            )
        );
        // Instragram URL
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'instagram-url',
                array(
                    'label'    => __( 'Instagram URL', '_s' ),
                    'section'  => 'social-media',
                    'settings' => 'instagram-url'
                )
            )
        );
        // Instragram Name
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'instagram-name',
                array(
                    'label'    => __( 'Instagram Name', '_s' ),
                    'section'  => 'social-media',
                    'settings' => 'instagram-name'
                )
            )
        );
        
    }
    add_action('customize_register', 'social_media');

    // Footer
    function footer_data( $wp_customize ) {
        // Settings
        $wp_customize->add_setting( 'email', array( 'default' => '' ) );
        $wp_customize->add_setting( 'footer_logo', array( 'default' => '' ) );

        // Sections
        $wp_customize->add_section(
            'footer-data',
            array(
                'title' => __( 'Footer', '_s' ),
                'priority' => 30,
                'description' => __( 'Enter the information to appear in the footer.', '_s' )
            )
        );

        // Controls
        // Email
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize, 'email',
                array(
                    'label' => __( 'Email', '_s' ),
                    'section' => 'footer-data',
                    'settings' => 'email'
                )
            )
        );

        // Footer logo
        $wp_customize->add_control(
            new WP_Customize_Image_Control(
                $wp_customize, 'footer_logo',
                array(
                    'label' => __( 'Footer logo', '_s' ),
                    'section' => 'footer-data',
                    'settings' => 'footer_logo'
                )
            )
        );
        
    }
    add_action('customize_register', 'footer_data');
?>