<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'daat');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-T|MEnU-|NprlKWZ(nv0R *5W:Z=T*.u]Wb}G*oc014CQa#.U&GJ}W|pvd_6m3v|');
define('SECURE_AUTH_KEY',  '?R,)2Sn]T+27Kj&lk )t|v.Xn.^=BF*UAI>C(s0 F6na}NFk@P %FGy.J+-]g/70');
define('LOGGED_IN_KEY',    'baig?[G38`xu(CP/Ye~yWFimBg;I*WMkn9+ZTv;ik~+u~zk-A)ma:R8xt`&,zWV6');
define('NONCE_KEY',        'DF$pLt.`zO%8~]_{m}NY*]:FYOjmrT|i,e>BNgt:a0P+O-hq*}|M(NIk!/iV&E-j');
define('AUTH_SALT',        '$dXZ6*mSg|xB!s@fwzduOK41+<~An87q<pJG. VY!}~S?(-$A/er+wEz2;lE<*7E');
define('SECURE_AUTH_SALT', '/m:>tb24So@s:Ijpb?v4E}nOT,hPB}#H*<@+*+VFNE-DTfO!+CaekbfGPg|Q?h!.');
define('LOGGED_IN_SALT',   'J?qrWk2IV^zm|I2jpZ)zCipYy-x9_7>w+Tw$E-iDx,s_Ay*d0<]FwIC|GVxC7#zp');
define('NONCE_SALT',       'cJ]oBQR[Lr>W%Uk)+Nq-|RdT&%5{1q}i2cr.F.<%K{Z+#Ba.0dZK|MUMA]mzNE8`');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix = 'daat_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

